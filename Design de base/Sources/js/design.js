//$(document).ready(function){
$(function(){

    var design = true;

    $("img" ).click(function() {

        if (design==true) {
            $("#content").css("background", "gray");

            $("p").css("color", "black");

            $("h1,h2:not('.anecdote')").css("color", "#C33");

            $('h1,h2').css('border-color', "#C33");

            $("h2.anecdote + p").css("color", "");

            $("h1").animate({
                width: 400,
            }, 3000, function () {
            });
            design = false;


        } else {

            $("#content").css("background", "");

            $("p").css("color", "");

            $("h1,h2:not('.anecdote')").css("color", "");

            $('h1,h2').css('border-color', "");

            $("h2.anecdote + p").css("color", "");

            $("h1").animate({
                width: 400,
            }, 3000, function () {
            });
            design = true;

        }

    });

        console.log(design);

});

